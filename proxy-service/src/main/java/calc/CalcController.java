package calc;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Response;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class CalcController {

   
    
    @RequestMapping("/calc")
    public int calc(@RequestParam(value="x", defaultValue="10") int x,@RequestParam(value="y", defaultValue="10") int y) {
    	/*
    	if(x== (int) x && y== (int) y) {
    		 return x+y;
    		
    	}
    	 //throw new IllegalArgumentException("");
    	return -999;
    	*/
    	  final String uri = "http://localhost:8080/calc/{x}/{y}";
    	  Map<String, Integer> params = new HashMap<String, Integer>();
    	    params.put("x", x);
    	    params.put("y", y);
    	    RestTemplate restTemplate = new RestTemplate();
    	    int result = restTemplate.getForObject(uri, Integer.class,params);
    	    
    	 return result;
    }
    
    @ExceptionHandler
    void handleIllegalArgumentException(
                      IllegalArgumentException e,
                      HttpServletResponse response) throws IOException {
 
        response.sendError(HttpStatus.BAD_REQUEST.value());
 
    }
    
}
