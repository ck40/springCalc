package calc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    	//start(Application.class).properties("server.port=${other.port:9000}").run(args);
    }
    /*
    private static SpringApplicationBuilder start(Class<?>... sources) {
        return new SpringApplicationBuilder(Application.class)
            .showBanner(false)
            .child(sources);
    }
    */
}
/*
 public static void main(String[] args) {
    start(Admin.class, Webshop.class).run(args);
    start(Another.class).properties("server.port=${other.port:9000}").run(args);
}

private static SpringApplicationBuilder start(Class<?>... sources) {
    return new SpringApplicationBuilder(Domain.class)
        .showBanner(false)
        .child(sources);
}
  
  
 */
