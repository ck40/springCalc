package calc;

public class Calculator {

	private int x;
	private int y;
	
	private static Calculator myCalc;
	
	private Calculator () {
		
	}
	
	public static Calculator getInstance() {
		if (myCalc==null) {
			myCalc= new Calculator();
		}
		return myCalc;
	}
	
	public int f(int x,int y)
	{
		return x+y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	
}
