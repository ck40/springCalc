package calc;

import org.springframework.web.bind.annotation.PathVariable;
//import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalcController {

   
    
    @RequestMapping("/calc/{x}/{y}")
    public int f(@PathVariable ("x") int x , @PathVariable ("y") int y) {
    	/*
    	if(x== (int) x && y== (int) y) {
    		 return x+y;
    	}
    	return -999;
    	*/
    	Calculator c= Calculator.getInstance();
    	
    	 return c.f(x, y);
    }
    
}
